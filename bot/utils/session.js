const QUIZ_HTTP_SDK = require("../../quiz-http-sdk");
const { shuffleString, createHintArray, randomIndexGenerator } = require("../utils/generic");
const {newLevel: newLevelResponse} = require("../response/az/quiz");

function checkCurrentQuestion() {
    const {
        session: {
            quiz: {
                currentQuestion
            }
        }
    } = this

    return !!currentQuestion.question;
}


async function getAndSetQuizData() {
    try {
        await getAndSetTopics.call(this);
    } catch (e) {
        throw e;
    }

    try {
        await getAndSetSubTopics.call(this);
    } catch (e) {
        throw e;
    }


    try {
        await getAndSetQuestions.call(this);
    } catch (e) {
        throw e;
    }
}


async function getAndSetTopics() {
    try {
        var topicsResponse = await QUIZ_HTTP_SDK.getTopics({});
    }catch (e) {
        throw new Error("HTTP_ERROR_ON_TOPICS");
    }

    const {
        data: topicsResponseData,
        current_page: topicsCurrentPage,
        from: topicsFirstPage,
        last_page: topicsLastPage
    } = topicsResponse;

    if(!topicsResponseData.length) {
        throw new Error(`TOPICS_IS_EMPTY`);
    }

    this.session.quiz.topics.data = topicsResponseData;
    this.session.quiz.topics.pagination = {
        currentPage: topicsCurrentPage,
        firstPage: topicsFirstPage,
        lastPage: topicsLastPage
    }
}

async function getAndSetSubTopics(index = 0) {
    const firstTopic = this.session.quiz.topics.data[index];

    try {
        var subTopicsResponse = await QUIZ_HTTP_SDK.getSubTopics({topicId: firstTopic.id});
    }catch (e) {
        throw new Error("HTTP_ERROR_ON_SUB_TOPICS");
    }


    const {
        data: subTopicsResponseData,
        current_page: subTopicsCurrentPage,
        from: subTopicsFirstPage,
        last_page: subTopicsLastPage
    } = subTopicsResponse;

    if(!subTopicsResponseData.length) {
        throw new Error(`SUB_TOPICS_IS_EMPTY`);
    }

    this.session.quiz.subTopics.data = subTopicsResponseData;
    this.session.quiz.subTopics.pagination = {
        currentPage: subTopicsCurrentPage,
        firstPage: subTopicsFirstPage,
        lastPage: subTopicsLastPage
    }
    this.session.quiz.currentSubTopic.index = 0;
}

async function getAndSetQuestions(index = 0) {
    const firstSubTopic = this.session.quiz.subTopics.data[index];

    try {
        var questionsResponse = await QUIZ_HTTP_SDK.getQuestions({subTopicId: firstSubTopic.id});
    }catch (e) {
        throw new Error("HTTP_ERROR_ON_QUESTIONS");
    }


    const {
        data: questionsResponseData,
        current_page: questionsCurrentPage,
        from: questionsFirstPage,
        last_page: questionsLastPage
    } = questionsResponse;

    if(!questionsResponseData.length) {
        throw new Error(`SUB_TOPICS_IS_EMPTY`);
    }

    this.session.quiz.questions.data = questionsResponseData;
    this.session.quiz.questions.pagination = {
        currentPage: questionsCurrentPage,
        firstPage: questionsFirstPage,
        lastPage: questionsLastPage
    }
}

async function setTopics() {
    if(this.session.quiz.currentTopic.index >= this.session.quiz.topics.data.length - 1) {

    }else {
        this.session.quiz.currentTopic.index = this.session.quiz.currentTopic.index + 1;
        await getAndSetSubTopics.call(this, this.session.quiz.currentTopic.index);
        this.session.quiz.currentSubTopic.index = -1;
        await setSubTopics.call(this);
    }
}

async function setSubTopics() {
    if(this.session.quiz.currentSubTopic.index >= this.session.quiz.subTopics.data.length - 1) {
        await setTopics.call(this);
    }else {
        this.session.quiz.currentSubTopic.index = this.session.quiz.currentSubTopic.index + 1;
        await getAndSetQuestions.call(this, this.session.quiz.currentSubTopic.index);
        this.session.quiz.currentQuestion.question = null;
        this.session.quiz.currentQuestion.index = 0;
        // await setCurrentQuestion();
    }
}


async function setCurrentQuestion() {

    let nextQuestionIndex;

    if(this.session.quiz.currentQuestion.question) {
        if(this.session.quiz.currentQuestion.index >= this.session.quiz.questions.data.length - 1) {
            await setSubTopics.call(this);
            await newLevelResponse.call(this);
            nextQuestionIndex = 0;
        }else {
            nextQuestionIndex = this.session.quiz.currentQuestion.index + 1;
        }
    }else {
        nextQuestionIndex = 0;
    }

    const firstQuestion = this.session.quiz.questions.data[nextQuestionIndex];

    this.session.quiz.currentQuestion.index = nextQuestionIndex;
    this.session.quiz.currentQuestion.question = firstQuestion.question;
    this.session.quiz.currentQuestion.answer = firstQuestion.answer;
    this.session.quiz.currentQuestion.shuffleAnswer = shuffleString(firstQuestion.answer);
    this.session.quiz.currentQuestion.hint = createHintArray(firstQuestion.answer.length)
}

function setHintLetter() {
    var index = randomIndexGenerator(0, this.session.quiz.currentQuestion.answer.length);
    if(this.session.quiz.currentQuestion.hint[index] === "*") {
        this.session.quiz.currentQuestion.hint[index] = this.session.quiz.currentQuestion.answer[index];
    }else {
        setHintLetter.call(this);
    }

}

module.exports = {
    checkCurrentQuestion,
    getAndSetQuizData,
    setCurrentQuestion,
    setHintLetter
}