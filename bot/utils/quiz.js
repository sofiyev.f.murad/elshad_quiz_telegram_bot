const {setCurrentQuestion} = require("../utils/session");
const {removeSpaceFromString} = require("../utils/generic");
const {askQuestion: askQuestionResponse} = require("../response/az/quiz");

async function askQuestion() {
    await setCurrentQuestion.call(this);
    askQuestionResponse.call(this);
}

function checkAnswerIsCorrect() {

    const {
        session: {
            quiz: {
                currentQuestion: {
                    answer
                }
            }
        },
        message: {
            text: message
        }
    } = this;

    const normalizedMessage = removeSpaceFromString(message.toLowerCase());
    const normalizedCorrectAnswer = removeSpaceFromString(answer.toLowerCase());

    return normalizedMessage === normalizedCorrectAnswer;
}


module.exports = {
    askQuestion,
    checkAnswerIsCorrect
}