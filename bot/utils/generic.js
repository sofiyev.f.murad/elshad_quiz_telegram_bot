function removeSpaceBetweenWords(string) {
    return string.replace(/ +(?= )/g,'');
}

function removeSpaceFromString(string) {
    return string.replace(/\s/g, '');
}

function shuffleString(string) {
    let shuffle = [...string];
    const getRandomValue = (i, N) => Math.floor(Math.random() * (N - i) + i);
    shuffle.forEach((elem, i, arr, j = getRandomValue(i, arr.length)) => [arr[i], arr[j]] = [arr[j], arr[i]]);
    shuffle = shuffle.join('');
    if(string === shuffle) {
        return shuffleString(string);
    }else {
        return shuffle;
    }

}

function createHintArray(length) {
    return new Array(length).fill(`*`);
}


function randomIndexGenerator(min, max) {
    return Math.floor(Math.random() * max) + min;
}

module.exports = {
    removeSpaceBetweenWords,
    removeSpaceFromString,
    shuffleString,
    createHintArray,
    randomIndexGenerator
}