async function initSession(ctx, next) {

    const {
        session,
        session: {
            user
        }
    } = ctx;

    if (user) {
        next();
        return;
    }

    session.quiz = {
        topics: {
            pagination: {},
            data: null
        },
        subTopics: {
            pagination: {},
            data: null
        },
        questions: {
            pagination: {},
            data: null
        },
        currentQuestion: {
            index: null,
            question: null,
            answer: null,
            hint: null
        },
        currentSubTopic: {
            index: null
        },
        currentTopic: {
            index: null
        }
    }
    session.user = {
        score: 0,
        contact: null
    }
    next();
}


module.exports = initSession;