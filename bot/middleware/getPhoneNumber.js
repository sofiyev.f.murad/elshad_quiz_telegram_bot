const {
    startPhoneNumberRequest: startPhoneNumberRequestResponse,
    finishPhoneNumberRequest: finishPhoneNumberRequestResponse
} = require("../response/az/middleware");

async function getPhoneNumber(ctx, next) {

    const {
        session,
        session: {
            user: {
                contact: sessionContact
            }
        },
        message: {
            contact: messageContact
        }
    } = ctx;

    if(sessionContact) {
        next();
        return;
    }

    if(messageContact) {
        session.user.contact = messageContact;
        finishPhoneNumberRequestResponse.call(ctx).then(() => {
            ctx.scene.enter("quiz");
        })
    }else {
        startPhoneNumberRequestResponse.call(ctx);
    }

}


module.exports = getPhoneNumber;