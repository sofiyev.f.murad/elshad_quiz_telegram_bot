require('dotenv').config();

const {Telegraf} = require('telegraf');
const Stage = require('telegraf/stage');
const session = require('telegraf/session');

const getPhoneNumberMiddleware = require("./middleware/getPhoneNumber");
const initSession = require("./middleware/initSession");

const quizScene = require("./scene/quiz");

const stage = new Stage([quizScene]);

const bot = new Telegraf(process.env.TELEGRAM_BOT_TOKEN);

bot.use(session());

bot.use(stage.middleware());

bot.use(initSession);

bot.use(getPhoneNumberMiddleware);


bot.startPolling().catch((error, ctx) => {
    console.log(error.message);
});

