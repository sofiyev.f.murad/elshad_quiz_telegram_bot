const Extra = require('telegraf/extra');
const Markup = require('telegraf/markup');

const {removeSpaceBetweenWords} = require("../../utils/generic");

function startQuiz() {
    this.reply(
        removeSpaceBetweenWords(`
              <b>Salam <i>Text Zone</i> oyununa xoş gəlmisiniz</b>\n
              <i>Oyuna başlamaq üçün başla düyməsini sıxın və ya /quiz əmrini göndərin</i>
        `),
        Extra.HTML().markup((m) =>
            Markup.inlineKeyboard([
                m.callbackButton(`* Başla *`, "start-quiz"),
            ]),
        )
    );
}

function askQuestion() {
    this.replyWithHTML(
        removeSpaceBetweenWords(`
             Sual: <b>${this.session.quiz.currentQuestion.question}</b>\n      
             Hint: <b> ${this.session.quiz.currentQuestion.shuffleAnswer} </b>
        `),
    );
}

function newLevel() {
    return this.replyWithHTML(
        removeSpaceBetweenWords(`
             Starting ${this.session.quiz.subTopics.data[this.session.quiz.currentSubTopic.index].sub_name} 🎉
        `),
    );
}

async function correctAnswer() {
    await this.replyWithHTML(
        removeSpaceBetweenWords(`
             <b>Doğru cavab ✅ </b>\n
             <i>Sizin toplam xalınız ${this.session.user.score}</i>      
        `),
    );
}

function wrongAnswer() {
    return this.reply('Kömək üçün aşağıdakı seçimlərdən birini edin', {
        reply_markup: {
            inline_keyboard: [
                [
                    {
                        text: `Hərf göstər`,
                        callback_data: "show-letter"
                    },
                    {
                        text: `Sözü göstər`,
                        callback_data: "show-answer"
                    },
                ]
            ],
            'resize_keyboard': true
        }
    });
}

function showLetter() {
    this.replyWithHTML(
        removeSpaceBetweenWords(`
             <b>${this.session.quiz.currentQuestion.hint.join("")}</b>
        `),
    );
}

function showAnswer() {
    this.replyWithHTML(
        removeSpaceBetweenWords(`
             <b>${this.session.quiz.currentQuestion.answer}</b>
        `),
    );
}

module.exports = {
    startQuiz,
    askQuestion,
    correctAnswer,
    wrongAnswer,
    showLetter,
    showAnswer,
    newLevel
}