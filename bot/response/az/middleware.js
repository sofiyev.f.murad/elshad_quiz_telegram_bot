function startPhoneNumberRequest() {
    return this.reply('Xaiş olunur oyuna başlamadan öncə nömrənizi bizim ilə paylaşın', {
        reply_markup: {
            keyboard: [[{
                text: '📲 Nömrəni göndər',
                request_contact: true
            }]]
        }
    });
}

function finishPhoneNumberRequest() {
    return this.reply("Təşəkkürlər", {reply_markup: {remove_keyboard: true}})
}

module.exports = {
    startPhoneNumberRequest,
    finishPhoneNumberRequest
}

