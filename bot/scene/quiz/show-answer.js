const {
    showAnswer:showAnswerResponse,
} = require("../../response/az/quiz");

async function showLetterHandler(ctx) {
    showAnswerResponse.call(ctx);
}

module.exports = showLetterHandler;