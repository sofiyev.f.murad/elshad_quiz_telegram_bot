const { startQuiz: startQuizResponse } = require("../../response/az/quiz");

async function enterHandler(ctx) {
    startQuizResponse.call(ctx);
}


module.exports = enterHandler;