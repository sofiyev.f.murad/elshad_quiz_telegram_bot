const {checkAnswerIsCorrect} = require("../../utils/quiz");
const {askQuestion} = require("../../utils/quiz");
const {
    wrongAnswer: wrongAnswerResponse,
    correctAnswer: correctAnswerResponse
} = require("../../response/az/quiz");

async function answerQuestionHandler(ctx) {
    if (checkAnswerIsCorrect.call(ctx)) {
        ctx.session.user.score = ctx.session.user.score + 1;
        correctAnswerResponse.call(ctx).then(() => {
            askQuestion.call(ctx);
        })
    } else {
        wrongAnswerResponse.call(ctx);
    }
}

module.exports = answerQuestionHandler;