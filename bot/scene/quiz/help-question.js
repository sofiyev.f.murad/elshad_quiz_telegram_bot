const {
    wrongAnswer: wrongAnswerResponse,
} = require("../../response/az/quiz");

async function answerQuestionHandler(ctx) {
    wrongAnswerResponse.call(ctx);
}

module.exports = answerQuestionHandler;