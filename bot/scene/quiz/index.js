const Scene = require('telegraf/scenes/base');

const quizScene = new Scene('quiz');

const enterHandler = require("./enter");
const startQuizHandler = require("./start-quiz");
const answerQuestionHandler = require("./answer-question");
const helpQuestionHandler = require("./help-question");
const showLetterHandler = require("./show-letter");
const showAnswerHandler = require("./show-answer");



quizScene.enter(enterHandler);

quizScene.action("start-quiz", startQuizHandler);

quizScene.action("show-letter", showLetterHandler);
quizScene.action("show-answer", showAnswerHandler);

quizScene.command("help", helpQuestionHandler);

quizScene.on("message", answerQuestionHandler);

module.exports = quizScene;