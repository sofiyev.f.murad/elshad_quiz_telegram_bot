const {
    showLetter:showLetterResponse,
    showWrongLetter: showWrongLetterResponse,
} = require("../../response/az/quiz");

const {
    setHintLetter
} = require("../../utils/session");

async function showLetterHandler(ctx) {
    setHintLetter.call(ctx);
    showLetterResponse.call(ctx);
}

module.exports = showLetterHandler;