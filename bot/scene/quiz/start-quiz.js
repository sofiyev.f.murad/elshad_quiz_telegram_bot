const { getAndSetQuizData } = require("../../utils/session");
const { askQuestion } = require("../../utils/quiz");

async function startQuizHandler(ctx) {
    try {
        await getAndSetQuizData.call(ctx);
    }catch (e) {
        console.log(e.message);
    }
    askQuestion.call(ctx);
}

module.exports = startQuizHandler