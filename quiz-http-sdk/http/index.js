const axios = require("axios");

const responseHandler = require("./responseHandler");
const errorHandler = require("./errorHandler");

const axiosInstance = axios.create({
    baseURL: process.env.BACKEND_BASE_URL,
});

axiosInstance.interceptors.response.use(responseHandler, errorHandler)

module.exports = axiosInstance;